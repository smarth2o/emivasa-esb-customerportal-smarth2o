#input user_id
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

Integer[] user_id = user_id
	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

query = "select dc.name, dc.number " +
		"from user u " +    
        "left outer join neutral_user nu on u.oid = nu.user_oid " +
        "left outer join household h on nu.household_oid = h.oid " +
        "left outer join device_class dc on h.oid = dc.household_oid " +
		"where nu.user_oid = " + user_id[0]

//println query

def result=null
def jsonString = null

result = session.createSQLQuery(query).list()

try {
    OutputStream out = new ByteArrayOutputStream() 
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    
    boolean existResult = false
    
    jGenerator.writeStartObject() // {
    
    jGenerator.writeFieldName("devices");
    jGenerator.writeStartArray()  // [
    for (r in result){
	    if(r[0].toString()!=null)
	    {
	    	//write json
	    	existResult = true
	    	jGenerator.writeStartObject() // {
		    jGenerator.writeStringField("name", r[0])
		    jGenerator.writeNumberField("number", r[1])
		    jGenerator.writeEndObject()   // }
	    }
	}
	jGenerator.writeEndArray() // ]

	if(existResult)
		jGenerator.writeNumberField("completion_percentage", 100)
	else
		jGenerator.writeNumberField("completion_percentage", 0)

	jGenerator.writeEndObject()  // }
	
    jGenerator.close()

    json = out.toString("UTF-8")

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

commit(session)

println json

json = out.toString("UTF-8")
}
return ["json" : json]
