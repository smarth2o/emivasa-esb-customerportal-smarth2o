#input user_id
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

query = "SELECT mr.total_consumption_adjusted, DATE_SUB(mr.reading_date_time, INTERVAL 1 DAY) " +
		"FROM meter_reading mr " +
		"LEFT OUTER JOIN household h ON mr.smart_meter_oid = h.smart_meter_oid  " +
		"LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid  " +
        "LEFT OUTER JOIN user u ON u.oid = nu.user_oid  " +
		"WHERE u.oid = \'" + user_id + "\' " +
		"GROUP BY DATE(mr.reading_date_time) " +
		"ORDER BY mr.reading_date_time DESC LIMIT 1 "

println query

def result=null

def ris=0
result = session.createSQLQuery(query).list()
def average = 0;

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()


    for (r in result){
    	//write json
    	jGenerator.writeStartObject()
	    jGenerator.writeNumberField("reading", new Double(r[0]).doubleValue())
	 	jGenerator.writeStringField("timestamp", r[1].toString())
	    jGenerator.writeEndObject()

	}

	jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString("UTF-8")

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)

println json
ArrayNode mapper = new ObjectMapper().readTree(json);
println mapper
return ["json" : mapper]