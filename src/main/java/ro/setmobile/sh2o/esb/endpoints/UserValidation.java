/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewSmartH2O/UserValidation")
public class UserValidation {

	@POST
	@Path("/validateUser")
	@Produces(MediaType.APPLICATION_JSON)
	public String validateUser(@FormParam("user_email") String user_email, @FormParam("user_password") String user_password) {
		return null;
	}
}
