/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewSmartH2O/GamificationData")
public class GamificationData {

	@POST
	@Path("/access/{version}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String getActions(@PathParam("version") String version, @QueryParam("gamification") String gamification) {
		return null;
	}
}
